package eg.edu.alexu.csd.oop.calculator;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.*;
public class GUICalculator  {
    iCalculator calculatorObject = new iCalculator();
    public GUICalculator() {




        resultBox.setText("0.0");
        resultBox.setEditable(false);
        nextButton.setEnabled(false);
        previousButton.setEnabled(false);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            String btn1Text =  btn1.getText();
            textBox.setText(textBox.getText()+btn1Text);
            }
        });
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn2Text =  btn2.getText();
                textBox.setText(textBox.getText()+btn2Text);
            }
        });
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn3Text =  btn3.getText();
                textBox.setText(textBox.getText()+btn3Text);
            }
        });
        btn4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn4Text =  btn4.getText();
                textBox.setText(textBox.getText()+btn4Text);
            }
        });
        btn5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn5Text =  btn5.getText();
                textBox.setText(textBox.getText()+btn5Text);
            }
        });
        btn6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn6Text =  btn6.getText();
                textBox.setText(textBox.getText()+btn6Text);
            }
        });
        btn7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn7Text =  btn7.getText();
                textBox.setText(textBox.getText()+btn7Text);
            }
        });
        btn8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn8Text =  btn8.getText();
                textBox.setText(textBox.getText()+btn8Text);
            }
        });
        btn9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn9Text =  btn9.getText();
                textBox.setText(textBox.getText()+btn9Text);
            }
        });
        btn0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btn0Text =  btn0.getText();
                textBox.setText(textBox.getText()+btn0Text);
            }
        });
        btnDot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnDotText =  btnDot.getText();
                textBox.setText(textBox.getText()+btnDotText);
            }
        });
        btnPlus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnPlusText =  btnPlus.getText();
                textBox.setText(textBox.getText()+btnPlusText);
            }
        });
        btnMinus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnMinusText =  btnMinus.getText();
                textBox.setText(textBox.getText()+btnMinusText);
            }
        });
        btnMlt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnMltText =  btnMlt.getText();
                textBox.setText(textBox.getText()+btnMltText);
            }
        });
        btnDiv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnDivText =  btnDiv.getText();
                textBox.setText(textBox.getText()+btnDivText);
            }
        });

        btnCalc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculatorObject.input(textBox.getText());
                if(calculatorObject.error){
                    JOptionPane.showMessageDialog(null, calculatorObject.message);

                }else {
                    resultBox.setText(calculatorObject.getResult());
                    setEnablity(calculatorObject.oVector,calculatorObject.operations);

                }
            }
        });


        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculatorObject.next();
                textBox.setText(calculatorObject.current);
                resultBox.setText(calculatorObject.getResult());
                setEnablity(calculatorObject.oVector,calculatorObject.operations);

            }
        });
        previousButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculatorObject.prev();
                textBox.setText(calculatorObject.current);
                resultBox.setText(calculatorObject.getResult());
                setEnablity(calculatorObject.oVector,calculatorObject.operations);

            }
        });

        textBox.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode()==KeyEvent.VK_ENTER){
                    calculatorObject.input(textBox.getText());
                    if(calculatorObject.error){
                        JOptionPane.showMessageDialog(null, calculatorObject.message);

                    }else {
                        resultBox.setText(calculatorObject.getResult());

                        setEnablity(calculatorObject.oVector,calculatorObject.operations);

                    }

                }
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {/*
                JFileChooser fileChooser = new JFileChooser();
                int rVal = fileChooser.showSaveDialog(saveButton);
                    if(rVal == JFileChooser.APPROVE_OPTION){
                        FileOutputStream fout = null;
                        ObjectOutputStream oos = null;

                        File file = fileChooser.getSelectedFile();
                        if (file == null) {
                            return;
                        }
                        if (!file.getName().toLowerCase().endsWith(".txt")) {
                            file = new File(file.getParentFile(), file.getName() + ".txt");
                        }
                        try {
                            fout = new FileOutputStream(file);

                        }catch (FileNotFoundException s) { s.printStackTrace(); }
                        try {
                            oos = new ObjectOutputStream(fout);

                            oos.writeObject(calculatorObject);

                            oos.close();

                        } catch (IOException r) { r.printStackTrace(); }


                        }
                JOptionPane.showMessageDialog(null, "File saved successfully.");
                */
                calculatorObject.save();
            }
        });
        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*
                JFileChooser fileChooser = new JFileChooser();
                int rVal = fileChooser.showOpenDialog(loadButton);
                if(rVal == JFileChooser.APPROVE_OPTION){
                    FileInputStream fout = null;
                    ObjectInputStream oos = null;

                    File file = fileChooser.getSelectedFile();
                    if (file == null) {
                        return;
                    }

                    try {
                        fout = new FileInputStream(file);

                    }catch (FileNotFoundException s) { s.printStackTrace(); }
                    try {
                        oos = new ObjectInputStream(fout);

                        calculatorObject = (iCalculator)oos.readObject();

                        oos.close();

                    } catch (IOException r) { r.printStackTrace(); }
                    catch (ClassNotFoundException c) {
                        System.out.println("sad");
                        c.printStackTrace();
                        return;
                    }
                    JOptionPane.showMessageDialog(null, "File loaded successfully.");
                    textBox.setText(calculatorObject.current);
                    setEnablity(calculatorObject.oVector,calculatorObject.operations);
                    resultBox.setText(calculatorObject.getResult());

                }*/
                calculatorObject.load();
                /*try {
                    FileInputStream fileIn = new FileInputStream("calc.ser");
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    calculatorObject = (iCalculator) in.readObject();
                    in.close();
                    fileIn.close();
                } catch (IOException i) {
                    i.printStackTrace();
                    return;
                } catch (ClassNotFoundException c) {
                    System.out.println("Calculator class not found");
                    c.printStackTrace();
                    return;
                }*/
                setEnablity(calculatorObject.oVector,calculatorObject.operations);
                textBox.setText(calculatorObject.operations[calculatorObject.oVector]);
                resultBox.setText(calculatorObject.getResult());

            }
        });
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textBox.setText("");
                resultBox.setText("0.0");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("GUICalculator");
        frame.setContentPane(new GUICalculator().GUICalculator);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private JTextField textBox;
    private JButton btnDot;
    private JButton btn1;
    private JButton btn7;
    private JButton btn4;
    private JButton btn8;
    private JButton btn5;
    private JButton btn2;
    private JButton btn0;
    private JButton btnPlus;
    private JButton btnMinus;
    private JButton btnMlt;
    private JButton btnDiv;
    private JButton btnClear;
    private JButton btn3;
    private JButton btn9;
    private JButton btn6;
    private JButton nextButton;
    private JButton previousButton;
    private JButton saveButton;
    private JButton loadButton;
    private JButton btnCalc;
    private JPanel GUICalculator;
    private JTextField resultBox;
    private JLabel Label;
    private JLabel operationLabel;
    public void setEnablity (int oVector,String [] operations){
        if(oVector==0){
            previousButton.setEnabled(false);
            if(operations[oVector+1]!=null)
                nextButton.setEnabled(true);
            else
                nextButton.setEnabled(false);


        }else if(oVector==4){
            nextButton.setEnabled(false);
            if(operations[oVector-1]!=null) {
                previousButton.setEnabled(true);
            }
            else
                previousButton.setEnabled(false);

        }else{
            if(operations[oVector+1]!=null)
            nextButton.setEnabled(true);
            else
                nextButton.setEnabled(false);
            previousButton.setEnabled(true);
        }

    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
