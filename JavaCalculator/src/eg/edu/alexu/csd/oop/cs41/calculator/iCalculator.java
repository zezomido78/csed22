package eg.edu.alexu.csd.oop.calculator;

import java.io.*;
import java.util.regex.Pattern;

public class iCalculator implements Calculator,java.io.Serializable {
    String[] operations = new String[5];
    public int oVector =0;
    String current = new String();
    String message = new String();
    boolean error = false;
    public void input (String s){
        error = false;
        if(Pattern.matches("[-]?\\d+[/][-]?[0]+",s) || Pattern.matches("[-]?\\d+[/][-]?[0]+.[0]+",s) )
        {
            error=true;
            System.out.println("Division by 0 is not possible and error is"+error);
           message = "Division by 0 is not possible";
        }
        else if(Pattern.matches("[-]?\\d+[+-/*][-]?\\d+",s) || Pattern.matches("[-]?\\d+[+-/*][-]?\\d+[.]\\d+",s) || Pattern.matches("[-]?\\d+[.]\\d+[+-/*][-]?\\d+",s) || Pattern.matches("[-]?\\d+[.]\\d+[+-/*][-]?\\d+[.]\\d+",s))
        {
            current=s;
            error=false;
            saveResult();
        }

        else {
            error=true;
            message="Wrong entering format,Please re-enter your operation.";
        }

    }
    public String getResult(){


        String num1 = new String();
        String num2 = new String();
        double a;
        double b;
        double sum=0;
        int i=0;
        if(current.charAt(i)=='-')
            num1 = num1 + current.charAt(i++);

        for(; current.charAt(i)!='*' &&  current.charAt(i)!='+' &&  current.charAt(i)!='-' &&  current.charAt(i)!='/' ; i++){
            num1 = num1 +  current.charAt(i);
        }
        char operation = current.charAt(i);
        i++;
        if(current.charAt(i)=='-')
            num2 = num2 + current.charAt(i++);

        for (; i<current.length();i++){
            num2 = num2+ current.charAt(i);
        }
        a=Double.valueOf(num1);
        b=Double.valueOf(num2);
        if(operation=='+')
            sum = a+b;
        else if(operation=='*')
            sum=a*b;
        else if(operation=='-')
            sum=a-b;
        else if(operation=='/') {
            sum = a / b;
        }


        return Double.toString(sum);


    }
    public void saveResult(){
        if(oVector<4) {
            if(operations[oVector]==null){
            operations[oVector]=current;
            if(oVector!=4)
                oVector++;
            }
            else {
                for(int i=oVector; i<5; i++){
                    if(operations[i]==null){
                        oVector=i;
                        break;
                    }
                }
                operations[oVector]=current;

            }
        }else if(operations[oVector]==null&&oVector==4)
        {
            operations[oVector]=current;
        }
        else {
            for(int i=0;i<4;i++)
                operations[i]=operations[i+1];
            operations[oVector]=current;

        }
    }
    public String current (){
        if (current.length()==0)
                return null;
        return current;

    }
    public String prev(){
        if(oVector>0){
            if(operations[oVector]==null) {
                oVector--;
                current = operations[--oVector];
            }
            else {
                current = operations[--oVector];
            }
            return current;
        }
        else {
            return null;
        }
    }
    public String next(){
        if(oVector<4 && operations[oVector+1]!=null)
        {
            current=operations[++oVector];
            return current;
        }else
            return null;
    }
    public void save(){
        try{
            FileOutputStream fileOut = new FileOutputStream("calc.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(oVector);
            out.writeObject(operations);
            out.writeObject(current);

            out.close();
            fileOut.close();
        }catch (IOException i) {
            i.printStackTrace();
        }
    }
    public void load(){
        try {
            FileInputStream fileIn = new FileInputStream("calc.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            oVector = (Integer) in.readObject();
            operations = (String[])  in.readObject();
            current = (String) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Calculator class not found");
            c.printStackTrace();
            return;
        }

    }






}
